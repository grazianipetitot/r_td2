#########################################
#
# Exercice : R graphics
#
# Auteur : Antoine Lamer, Mathilde Fruchart
#
#########################################

## Chargement des packages
library(dplyr)
library(ggplot2)


## Chemins
source("path.R")

# ------------------------------------------------------------------------------------------

list.files(path_import)


############################################################################################
# 1 Chargement des données et data management
############################################################################################

data_src = read.csv2("data_220929.csv")
data_src$sexe = factor(data_src$sexe)
data_src$asa = factor(data_src$asa)
levels(data_src$asa) = c("ASA1", "ASA2", "ASA3", "ASA4", "ASA5")

##data_src$mois_fr = ordered(data_src$mois_fr, levels=c("Jan", "Fév", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", "Sep", "Oct", "Nov", "Dec"))

## Fin du data management

data = data_src


############################################################################################
# Consignes 
############################################################################################

# Vous pouvez rajouter des opérations de data management
# pour calculer des nouvelles variables
# Si des variables présentent des valeurs aberrantes, vous pouvez les filter
# Ex : retirer les poids > 200 kg
# Les graphiques doivent être lisibles et interprétables

# Q1 : Histogramme du poids
data %>%
  filter(poids < 150) %>%
  ggplot(aes(x=poids)) + 
  geom_histogram(aes(y=..density..))

# Q2 : Histogramme du poids avec un axe des absisses comprenant les valeurs entre 0 et 150
data %>%
  filter(poids < 150) %>%
  ggplot(aes(x=poids)) + 
  geom_histogram(aes(y=..density..))

data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids)) + 
  geom_histogram(aes(y=..density..)) +
  xlim(0, 150)

# Q4 : Courbe de densité du poids en fonction du sexe
# supprimer au préalable les poids > 200


# Q5 : Q4 + thème avec un fond blanc


# Q6 Boxplots représentant la durée d'anesthésie par classe ASA


# Q7 Représentation du nombre d'intervention par classe d'âge :
# [0-17[, [18-34[, [35-54[, [55-74[, >74


# Q8 : Q7 avec proposition de nouvelles couleurs


# Q9 : Q8 avec titre du graphique "Nombre d'interventions par classe d'âge"
# Retirer les libellés sur axes des abscisses et des ordonnées 


# Q10 Représentation de la mortalité par sexe


# Q11 Représentation de l'IMC en fonction de la classe ASA
# IMC = Poids / Taille (m) ²


# Q12 Représentation de la mortalité en fonction d'un IMC > 30 kg/m²


# Q13 Réprésenter la mortalité en fonction de IMC > 30 kg/m² et categorie_asa

